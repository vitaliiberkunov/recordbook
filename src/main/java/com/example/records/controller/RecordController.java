package com.example.records.controller;

import com.example.records.entity.RecordBook;

import com.example.records.services.RecordServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;


@RestController

public class RecordController {
    @Autowired
    private RecordServiceImpl service;


    @GetMapping("/records")
    public ResponseEntity<Object> getAllRecord() {
        return new ResponseEntity<>(service.findAllRecord(), HttpStatus.OK);
    }


    @GetMapping("/records/{lastname}")
    public ResponseEntity<Object> getNewsByTitle(@PathVariable(value = "lastname") String lastname) throws Exception {
        RecordBook book = service.getRecordByLastName(lastname);

        return new ResponseEntity<>(book, HttpStatus.OK);
    }


    @PostMapping("/records")
    public ResponseEntity<Object> create(@RequestBody RecordBook record) {
        service.save(record);
        return new ResponseEntity<>("Record is created successfully", HttpStatus.CREATED);
    }


    @PutMapping("/records/{id}")
    public ResponseEntity<Object> update(
            @PathVariable("id") Long id,
            @RequestBody RecordBook record
    ) {
        service.update(record,id);
        return new ResponseEntity<>("Record is updated successsfully", HttpStatus.OK);
    }


    @DeleteMapping("/news/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
        service.delete(id);
        return new ResponseEntity<>("Record is deleted successsfully", HttpStatus.OK);
    }



}

