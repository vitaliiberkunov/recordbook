package com.example.records.services;

import com.example.records.entity.RecordBook;

import java.util.List;

public interface RecordService {
    List<RecordBook> findAllRecord();
    void save(RecordBook recordBook);
    void delete (Long id);
    void update (RecordBook book, Long id);
    RecordBook getRecordByLastName(String lastName) throws Exception;


}
