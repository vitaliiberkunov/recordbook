package com.example.records.services;


import com.example.records.entity.RecordBook;
import com.example.records.repo.RecordBookRepo;
import org.springframework.beans.BeanUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class RecordServiceImpl implements RecordService {
    @Autowired
    private RecordBookRepo repo;

    @Override
    public List<RecordBook> findAllRecord() {

        return repo.findAll();
    }

    @Override
    public void save(RecordBook recordBook) {
        repo.save(recordBook);

    }

    @Override
    public void delete( Long id) {

        repo.deleteById(id);

    }

    @Override
    public void update(RecordBook book, Long id) {

        RecordBook record= repo.findById(id).orElseThrow(null);
        BeanUtils.copyProperties(book, record, "id");
        repo.save(record);

    }

    @Override
    public RecordBook getRecordByLastName(String lastName)  {
        return  repo.findRecordBookByLastName(lastName);
    }
}
