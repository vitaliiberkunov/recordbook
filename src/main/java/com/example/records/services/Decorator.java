package com.example.records.services;

import com.example.records.entity.Config;
import com.example.records.entity.RecordBook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
@Primary
public class Decorator implements RecordService {
    @Autowired
    private RecordService wrappe;

    private Config config;



    @Override
    public List<RecordBook> findAllRecord() {
        return wrappe.findAllRecord();
    }

    @Override
    public void save(RecordBook recordBook) {
        if(config.allow(recordBook)) {
            wrappe.save(recordBook);
        }
    }

    @Override
    public void delete(Long id) {
        wrappe.delete(id);
    }

    @Override
    public void update(RecordBook book, Long id) {
        wrappe.update(book, id);
    }

    @Override
    public RecordBook getRecordByLastName(String lastName) throws Exception {
        return wrappe.getRecordByLastName(lastName);
    }
}
