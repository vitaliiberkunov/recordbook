package com.example.records.entity;

public interface Prototype {
    public Object getClone();
}
