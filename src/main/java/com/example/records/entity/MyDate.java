package com.example.records.entity;


import lombok.*;
import javax.persistence.Id;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;



@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Entity

public class MyDate {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private int day;
    private int month;
    private int year;


    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }
}

