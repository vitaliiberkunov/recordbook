package com.example.records.entity;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.example.records.entity.RecordBook;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@AllArgsConstructor
public class Config {
    private List<RecordBook> notAllowed;

    public Config() {
        this.notAllowed = new ArrayList<RecordBook>(Arrays.asList(
               RecordBook.builder()
                .firstName("Коля")
                .lastName("Доценко")
                .phone("049406846")
                .build(),
                RecordBook.builder()
                .firstName("настя")
                .lastName("кукушкина")
                .phone("046909586")
                .build()
        ));
    }

    public boolean allow(RecordBook book) {

        for (RecordBook nAllow: notAllowed) {
            if(nAllow.equals(book)){
                return false;
            }

        }
        return true;
    }
}
