package com.example.records.repo;

import com.example.records.entity.RecordBook;
import org.springframework.data.jpa.repository.JpaRepository;


import java.util.List;


public interface RecordBookRepo extends JpaRepository<RecordBook,Long> {
    RecordBook findRecordBookByLastName(String lastName);
    List<RecordBook> findAll();



}
